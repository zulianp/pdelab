# Lecture 2: A parallel implementation of the Poisson problem

2 x 45 minutes lecture

## Block 1 (45 minutes)


|Time   |  Topic                                        | Details                                                                           |
|---    |---                                            |---                                                                                |
| T + 0 | Homework discussion       |                     Show solution, Q & A                                                      |
|---    |---                                            |---                                                                                |
| T + 15 |Sparse iterative solvers                        CRS matrix format, matrix-free, Jacobi iteration, method                                 |
|---    |---                                            |---                                                                                |
| T + 40| Q & A                             |              Questions on lecture
|---    |---                                            |---                                                                                |
| T + 45| 15 minutes break                              |                                                                                   |


## Block 2 (45 minutes)

|Time   |  Topic                                        | Details                                                                           |
|---    |---                                            |---                                                                                |
| T + 0 | Code presentation: Laplace operator             Explanation and live demo                                 |
|---    |---                                            |---                                                                                |
| T + 20| Matrix-free operator                             Main differences
|---    |---                                            |---                                                                                |
| T + 25| In-class homework                              | Implement boundary conditions and right-hand side 
|---    |---                                            |---                                                                                |
| T + 40| Administrative, Q & A                                 |                                                 
|---    |---                                            |---                                                                                |
| T + 45| End lecture                                   |                                                                                   |