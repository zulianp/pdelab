# Lecture 2: A parallel implementation of the Poisson problem

## Example codes

See `examples/poisson` for an example of a simple matrix-based version of a Poisson solver in the unit square.

## Resources

- Compressed Row Storage (CRS) or Compressed Sparse Row (CRS) matrix format: [https://de.wikipedia.org/wiki/Compressed_Row_Storage](https://de.wikipedia.org/wiki/Compressed_Row_Storage)
- Iterative solvers: [https://web.stanford.edu/class/cme324/saad.pdf](https://web.stanford.edu/class/cme324/saad.pdf)
- YouTube videos:
    + Iterative solver, Matrix formats (helpful) [https://www.youtube.com/watch?v=_3YRse8yUv0&ab_channel=BostonUniversity](https://www.youtube.com/watch?v=_3YRse8yUv0&ab_channel=BostonUniversity)
    + Sparse matrix Vector multilpicaton, Performance, Coalescing (more advanced and optional) [https://www.youtube.com/watch?v=74cPZO_4Edw&ab_channel=BostonUniversity](https://www.youtube.com/watch?v=74cPZO_4Edw&ab_channel=BostonUniversity)
