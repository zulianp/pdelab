# Lecture 1: Getting started


# Software dependencies

- [Git](https://git-scm.com/)
- [CMake](https://cmake.org/)
- [Kokkos](https://github.com/kokkos/kokkos)
- [Kokkos-kernels](https://github.com/kokkos/kokkos-kernels)


## Installing Git and CMake (It can be skipped on our cluster)
You will need `git` on your system for versioning your assignments.  
You can test if you have git with the following command in your terminal `git --version`.

Otherwise `git` can be installed with any package manager. For instance with [brew](https://brew.sh/) type `brew install git` in your terminal.

Similarly, CMake can be installed with `brew install cmake`.


## Kokkos

Kokkos is a hardware-portable (e.g. Multi-core CPUs and GPUs) library designed for node-level parallelism (threads).

### Step 1
```bash
# This series of commands are designed for a cluster environment, but they can be adapted (with some work) to your laptop environments. You need to load these modules every time you log-in to the cluster.

module purge
module load cudatoolkit/10.2 cmake git python
module list

export PDELAB_DIR=$HOME/pdelab
export DEPS_DIR=$PDELAB_DIR/deps
export KOKKOS_DIR=$DEPS_DIR/kokkos_install 
export COURSE_MATERIAL_DIR=$PDELAB_DIR/course_material

# All compilations have to be done on GPU nodes directly
# To start working or if you get kicked-out from the node, run the following command:
salloc -p debug-gpu


mkdir -p $DEPS_DIR
cd $DEPS_DIR
git clone https://github.com/kokkos/kokkos.git
git clone https://github.com/kokkos/kokkos-kernels.git


cd $DEPS_DIR/kokkos && mkdir $DEPS_DIR/kokkos/build
cd $DEPS_DIR/kokkos/build \
&& cmake .. -DKokkos_ARCH_PASCAL61=ON \
            -DKokkos_ENABLE_CUDA=ON \
            -DKokkos_ENABLE_CUDA_CONSTEXPR=ON \
            -DKokkos_ENABLE_CUDA_LAMBDA=ON \
            -DKokkos_ENABLE_DEBUG=ON \
            -DKokkos_ENABLE_OPENMP=ON  \
            -DKokkos_ENABLE_SERIAL=ON \
            -DKokkos_ENABLE_EXAMPLES=ON \
            -DCMAKE_INSTALL_PREFIX=$KOKKOS_DIR \
            -DCMAKE_BUILD_TYPE=Release \
&& make -j && make install

# Quick sanity check
OMP_PROC_BIND=true nvprof $DEPS_DIR/kokkos/build/example/tutorial/01_hello_world_lambda/KokkosExample_tutorial_01_hello_world_lambda

cd $DEPS_DIR/kokkos-kernels
mkdir $DEPS_DIR/kokkos-kernels/build
cd $DEPS_DIR/kokkos-kernels/build \
&& cmake .. -DKokkos_DIR=$KOKKOS_DIR/lib64/cmake/Kokkos \
            -DKokkosKernels_ENABLE_EXAMPLES=ON \
            -DCMAKE_INSTALL_PREFIX=$KOKKOS_DIR \
            -DCMAKE_BUILD_TYPE=Release \
&& make -j && make install

# Quick sanity check
OMP_PROC_BIND=true nvprof $DEPS_DIR/kokkos-kernels/build/example/wiki/sparse/KokkosKernels_wiki_spgemm
```

### Step 2

Donwload this repository
```bash
cd $COURSE_MATERIAL_DIR/..
git clone https://zulianp@bitbucket.org/zulianp/pdelab.git $COURSE_MATERIAL_DIR
cd $COURSE_MATERIAL_DIR
git pull
ls -lah

```

### Step 3

Test your environment!
```bash
cd $COURSE_MATERIAL_DIR/examples/hello_world/
mkdir build
cd build
cmake .. -DKokkosKernels_DIR=$KOKKOS_DIR/lib64/cmake/KokkosKernels \
&& make && OMP_PROC_BIND=true nvprof ./hello_world
```

# Lets start coding!

See below useful tools.


# Useful tools for cluster computing

Most of your interaction with the cluster will be performed in your `terminal` application.


## Secure shell (ssh)

Connect to the server
`ssh username@server`

Insert your password as prompted


## Secure copy (scp)

- Copying files from laptop to cluster: `scp path/to/my_local_file username@server:/path/to/my_remote_file`
- Copying files from cluster to laptop: `scp username@server:/path/to/my_remote_file path/to/my_local_file`

## Ssh file-system

You can create a network volume to access the cluster filesystem trough your system GUI and tools.

```bash
sshfs username@server:/home/username ~/Desktop/cluster_disk
```

You can install `sshfs` with any package manager (e.g. `brew install sshfs`).


## Customize your bash profile

Open your bash profile (e.g. `nano ~/.bash_profile`)


Add the following code to your bash profile
```bash
# Autocomplete 
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

# Load modules rquired for the PDELab class
module load cudatoolkit/10.2 cmake git python
```

You can create aliases to common commands and add them too. For instance
```bash
alias connect_to_cluster='sshfs username@server:/home/username ~/Desktop/cluster_disk'
```

Now you can just call `connect_to_cluster` instead of the whole command.


## Launching jobs on cluster

[Cluster documentation](https://intranet.ics.usi.ch/HPC)

We will be working mostly with GPU nodes. 
In order to compile and debug your code you need to ask for resources with `salloc -p debug-gpu`

For launching actual jobs use **sbatch files** with `sbatch my_sbatch_file.sbatch`. 
See examples sbatch files in the `examples` folder.

