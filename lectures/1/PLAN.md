# Lecture 1: Getting started

2 x 45 minutes lecture

## Block 1 (45 minutes)


|Time   |  Topic        								| Details                                 											|
|---    |---            								|---                                     							 				|
| T + 0 | Personal presentation (lecturer & student)    | Name, Accademic path, Goals. 		  												|
|---    |---            								|---                                      											|
| T + 5 | Introduction 									| Course structure, parallel computing model, toolchain.						    |
|---    |---            								|---                                      											|
| T + 15| Code presentation           					| Basics on C/C++, Kokkos															|
|---    |---            								|---                                      											|
| T + 40| Q & A											|																					|
|---    |---            								|---                                      											|
| T + 45| 15 mintues break								|																					|


## Block 2 (45 minutes)

|Time   |  Topic        								| Details                                 											|
|---    |---            								|---                                     							 				|
| T + 0 | Installation of tools    						| Tutorial and live demo on how to use the cluster  								|
|---    |---            								|---                                      											|
| T + 30| In class homework 							| Students work in class on their homework with supervision						    |
|---    |---            								|---                                      											|
| T + 40| Administrative           						| Homework constraints, next lecture												|
|---    |---            								|---                                      											|
| T + 45| End lecture									|																					|