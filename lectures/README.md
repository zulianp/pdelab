# Lectures

Welcome to the High-Performance Computing (HPC) portion of the PDE Lab.

This part of the course has the following structure:

1. **Getting started! (November the 25th)**
    - Introduction: **compute** model and **tool-chain**.
    - Code presentation: basic examples, programming concepts (C/C++), and scripts.
    - In class work: installation of library dependencies.
    - In class work: compiling and running the first parallel code samples.
        + Basic cluster orientation
        + Launching Jobs
        + Transfer data from laptop to cluster and vice-versa
    - Homework: implementation of a first hardware-portable parallel program.
2. **A parallel implementation of the Poisson problem (December the 2nd)**
    - Introduction: **matrix-based** and **matrix-free** operators with the **finite difference method**.
    - Iterative solution methods: **the Jacobi method**
    - Code presentation: Matrix-based Laplace operator, using finite differences
    - Code presentation: Dirichlet boundary conditions and forcing functions
    - Homework: Matrix-free Poisson solver using finite-differences
3. **Solution of linear systems (December the 9th)**
    - Discussion on the assignments
    - Iterative solution methods: 
        + The **red-black Gauss-Seidel method** (matrix-based)
        + Code presentation
    - Homework: Matrix-free red-black Gauss-Seidel solver
4. **A parallel implementation of the heat-equation (December the 16th)**
    - Discussion on the assignments
    - Code presentation: time-integration
    - Course **Project Kick-off**.
        + Monodomain model with IMEX time integration 
        + (Open for student proposals)
    - Homework: Heat-equation solver
5. **Course Project support session (December the 23rd)**
    - Students give a 5 minutes presentation
    - Supervised project work and Q & A session.
6. **Exam (Date TBA)**
    - Presentation 10 minutes (public)
    - Interrogation and discussion 5 minutes (private)

## Disclaimer

The content is subject to change.