# Lecture 4: A parallel implementation of the heat-equation

2 x 45 minutes lecture

## Block 1 (45 minutes)


|Time   |  Topic                                        | Details                                                                           |
|---    |---                                            |---                                                                                |
| T + 0 | Homework discussion                           | Show solution, Q & A                                                      		|
|---    |---                                            |---                                                                                |
| T + 25| Time integration                              | Refresh and code presentation                                                                                  ||---    |---                                            |---                                                                                |
|---    |---                                            |---                                                                                |
| T + 45| 15 minutes break                              |                                                                                   |


## Block 2 (45 minutes)

|Time   |  Topic                                        | Details                                                                           |
|---    |---                                            |---                                                                                |
| T + 0 | Project Kickoff                               | Directives and project discussion                                                                     |
|---    |---                                            |---                                                                                |
| T + 30| In-class homework                             | Implement algorithm                                                               |
|---    |---                                            |---                                                                                |
| T + 40| Administrative, Q & A                         |                                                                                   |
|---    |---                                            |---                                                                                |
| T + 45| End lecture                                   |                                                                                   |
