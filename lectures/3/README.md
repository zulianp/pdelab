# Lecture 3: The red-black Gauss-Seidel method

## Example codes

See `examples/red_black_gauss_seidel` for an example of a simple matrix-based version of a Poisson solver in the unit square.


## Resources

- [How to SpMV](https://arxiv.org/pdf/1307.6209.pdf)
- [Lecture slides on iterative methods and sparse linear algebra](https://www.cs.cornell.edu/~bindel/class/cs5220-s10/slides/lec14.pdf)
- Papers with RBGS example implementations on GPU (Advanced):
	+ [Fast GPU algorithms for implementing the red-black Gauss-Seidel method for Solving Partial Differential Equations](https://ieeexplore.ieee.org/document/6754958)
	+ [Accelerating the red/black SOR method using GPUs with CUDA](https://www.researchgate.net/publication/233730902_Accelerating_the_redblack_SOR_method_using_GPUs_with_CUDA)
