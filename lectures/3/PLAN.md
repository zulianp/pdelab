# Lecture 3: The red-black Gauss-Seidel method

2 x 45 minutes lecture

## Block 1 (45 minutes)


|Time   |  Topic                                        | Details                                                                           |
|---    |---                                            |---                                                                                |
| T + 0 | Homework discussion                           | Show solution, Q & A                                                      		|
|---    |---                                            |---                                                                                |
| T + 45| 15 minutes break                              |                                                                                   |


## Block 2 (45 minutes)

|Time   |  Topic                                        | Details                                                                           |
|---    |---                                            |---                                                                                |
| T + 0 | Red-black Gauss-Seidel                        | Introduction                                                                     |
|---    |---                                            |---                                                                                |
| T + 15| Code presentation: Red-black Gauss-Seidel     | Explanation and live demo                                 						|
|---    |---                                            |---                                                                                |
| T + 30| In-class homework                             | Implement algorithm                                                               |
|---    |---                                            |---                                                                                |
| T + 40| Administrative, Q & A                         |                                                                                   |
|---    |---                                            |---                                                                                |
| T + 45| End lecture                                   |                                                                                   |
