# PDELab #

In this repository you find resources and content for the HPC portion of the PDELab course.

### Lectures ###
See `lectures` folder for lecture content

### Example ###
Coding `examples` with CMake build scripts.

### Resources ###
* [Kokkos Wiki](https://github.com/kokkos/kokkos/wiki)
* [Kokkos Lectures](https://github.com/kokkos/kokkos-tutorials/wiki/Kokkos-Lecture-Series)
* [Kokkos-Kernels Wiki](https://github.com/kokkos/kokkos-kernels/wiki)


### Windows Users ###

In order to connect with SSH to our computing cluster install the following software:

Option 1)

- [https://winscp.net/eng/download.php](https://winscp.net/eng/download.php)
- [https://www.putty.org/](https://www.putty.org/)

Option 2)

- [https://www.cygwin.com/](https://www.cygwin.com/)