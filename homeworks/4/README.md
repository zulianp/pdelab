# Homework 4: A parallel implementation of the heat-equation

## Deadline
Before the next lecture

## Description

In this assignment you will be implementing a matrix-free Heat-equation solver in the unit square domain. 
The integration method will be the **Implicit Euler** method.
The solution method will be the **red-black Gauss-Seidel method**.

Perform the following basic tasks (see **TODO** in the code). You have to **deliver** both **code** and the specified **output files**.

### Part 1

1. Implement the following routines as described in the **TODO** comments in the code
    + Implement a red-black Gauss-Seidel step for computing the correction of the `Implicit Euler` scheme. 
2. Run the code from your build folder:
    + Use the `source ../run.sh`.
        *  If you are on your Laptop you can also generate a movie with this script (install `ffmpeg` from https://www.ffmpeg.org/ or use brew/macports).  
        *  On the compute nodes of the cluster (i.e., you have used `salloc`) `ffmpeg` is not available.
    + The experiment is performed on `30 x 30` grid for `100` time-steps.
3. Deliver a **1 page PDF document** where you present figures for the different time-steps 0, 24, 49, 74, 99 (i.e., using figures heateq_00000.png, heateq_00024.png, heateq_00049.png, ...) in a tidy way. For each of the figures report the value of `WightedDistanceToAnalyticStationarySolution`.
4. Deliver a **.txt** file with the output log of your run.

**Hints**

- The simulation will eventually converge to the stationary solution (i.e., the solution of the Poisson's problem). Use a larger end-time if necessary (time_frame=1 should be enough).



### Part 2

The project will serve as a final examination for the course together with a presentation in front of the class short interrogation with the examiners.

**Option 1** 

- **Monodomain model** with **IMEX time integration** with **Neumann boundary conditions**
- With **2 or more extensions** from the following list
    * Heterogeneous parameters
    * Non-uniform mesh
    * 3D version
    * Periodic boundary

**Option 2** 

- Student personal project (discussed in advance with lecturers)
    

**Tasks** 

1. Prepare a **5 minutes** presentation discussing the following topics (**maximum 1 slide per topic**)
    + Description of the project (i.e., which Option and extensions)
    + Technical details (i.e., list algorithms, formulas for the solution method, integration scheme, additional technology used)
    + Timeline with features (i.e., Milestones for the project).
    + Challenging/unclear parts and possible solutions.
2. Upload a **PDF document** of the presentation to icorsi.
