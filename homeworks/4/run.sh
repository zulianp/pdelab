nx=30
ny=30
nt=100

# Create directories for temporary output
mkdir ./output
mkdir ./figures

# Clean up
rm ./output/*.raw
rm ./figures/*.png

# Run executable
echo './hw_4 '$nx' '$ny
OMP_PROC_BIND=true ./hw_4 $nx $ny $nt

# Create movie (install ffmpeg from https://www.ffmpeg.org/)
python3 ../../../examples/utils/plot.py  --path=./output --nx=$nx --ny=$ny --output=./figures --batch

rm ./movie.mp4; ffmpeg -f image2 -pattern_type glob -i './figures/*.png' -c:v libx264 -vf fps=25 -pix_fmt yuv420p movie.mp4