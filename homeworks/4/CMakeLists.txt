cmake_minimum_required(VERSION 3.5...3.19)
cmake_policy(SET CMP0048 NEW)

include(${CMAKE_SOURCE_DIR}/../cmake/Macros.cmake)

project(
    hw_4
    VERSION 0.0.1
    LANGUAGES "CXX"
    HOMEPAGE_URL "https://bitbucket.org/zulianp/pdelab/"
    DESCRIPTION
        "Homework 4: Heat equation"
)

make_project(hw_4 main.cpp)

