if(NOT CMAKE_BUILD_TYPE)

    set(CMAKE_BUILD_TYPE
        "Release"
        CACHE STRING "Choose the type of build, options are: Debug Release
RelWithDebInfo MinSizeRel." FORCE)

    message(STATUS "[Status] CMAKE_BUILD_TYPE=Release")

endif(NOT CMAKE_BUILD_TYPE)


macro(make_project name main_file)

    # # Std CMake imports
    include(CMakePackageConfigHelpers)

    add_executable(${name} ${main_file})

    find_package(KokkosKernels REQUIRED)

    if(KokkosKernels_FOUND)
        if (TARGET Kokkos::kokkoskernels)
            target_link_libraries(${name} Kokkos::kokkoskernels)
            message(STATUS "Kokkos::kokkoskernels")
        else()
            target_include_directories(${name} PUBLIC ${KokkosKernels_TPL_INCLUDE_DIRS}
                                                           ${KokkosKernels_INCLUDE_DIRS})

            if(Kokkos_CXX_COMPILER)
                target_link_libraries(${name} ${KokkosKernels_LIBRARIES}
                                      ${KokkosKernels_TPL_LIBRARIES})
            else()
                target_link_libraries(
                    ${name} ${KokkosKernels_LIBRARIES} ${KokkosKernels_TPL_LIBRARIES}
                    -L${KokkosKernels_LIBRARY_DIRS})
            endif()
        endif()
    endif()

    if(KokkosKernels_C_COMPILER)
        set(CMAKE_C_COMPILER "${KokkosKernels_C_COMPILER}")
    endif()

    if(KokkosKernels_CXX_COMPILER)
        set(CMAKE_CXX_COMPILER "${KokkosKernels_CXX_COMPILER}")
    endif()

    message(STATUS
        "\nPacakge variables:\n"
        "CMAKE_C_COMPILER=${CMAKE_C_COMPILER}\n"
        "CMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}\n"
        "KokkosKernels_INCLUDE_DIRS=${KokkosKernels_INCLUDE_DIRS}\n"
        "KokkosKernels_TPL_LIBRARIES=${KokkosKernels_TPL_LIBRARIES}\n"
        "KokkosKernels_CXX_COMPILER=${KokkosKernels_CXX_COMPILER}\n"
        "KokkosKernels_C_COMPILER=${KokkosKernels_C_COMPILER}\n"
        )


    target_include_directories(${name} PUBLIC ../../examples/utils)


endmacro()