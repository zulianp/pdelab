
#include "base.hpp"

#include "KokkosBlas1_axpby.hpp"
#include "KokkosBlas1_nrm2.hpp"

#include <fstream>

using Real = double;

using ViewDevice2D = Kokkos::View<Real **, DeviceMemorySpace>;

// Analytic function
KOKKOS_INLINE_FUNCTION Real fun(const Real x, const Real y) {
  return (x * (1 - x) * y * (1 - y)) * (x + y);
}

// Analytic Laplacian
KOKKOS_INLINE_FUNCTION Real lapl_fun(const Real x, const Real y) {
  return 2 * (y - 1) * y * (3 * x + y - 1) + 2 * (x - 1) * x * (3 * y + x - 1);
}

int main(int argc, char *argv[]) {
  Kokkos::initialize(argc, argv);

  ////////////////////////////////////////////////////////

  {
    Kokkos::Timer t;
    Real start = t.seconds();

    // Grid parameters
    int nx = 100;
    int ny = 100;

    if (argc == 3) {
      nx = atoi(argv[1]);
      ny = atoi(argv[2]);
    }

    int margin = 1;

    int start_x = margin;
    int end_x = nx - margin;

    int start_y = margin;
    int end_y = ny - margin;

    // Geometry (unit square)
    Real hx = 1. / (nx - 1);
    Real hy = 1. / (ny - 1);

    // Solver parameters
    int max_iter = 20 * nx * ny;
    Real atol = 1e-8;
    int check_residual_each = 500;

    ////////////////////////////////////////////////////////////
    // Memory allocation
    ////////////////////////////////////////////////////////////

    ViewDevice2D x_device("XOnDevice", nx, ny);
    ViewDevice2D rhs_device("RHSOnDevice", nx, ny);
    Kokkos::deep_copy(x_device, 0.);

    ////////////////////////////////////////////////////////////
    // Boundary conditions
    ////////////////////////////////////////////////////////////

    Kokkos::parallel_for(
        "BoundaryConditions_X", DeviceRangeRank1(0, nx), KOKKOS_LAMBDA(int i) {
          Real x = i * hx;
          x_device(i, 0) = fun(x, 0);
          x_device(i, ny - 1) = fun(x, (ny - 1) * hy);
        });

    Kokkos::parallel_for(
        "BoundaryConditions_Y", DeviceRangeRank1(0, ny), KOKKOS_LAMBDA(int j) {
          Real y = j * hy;
          x_device(0, j) = fun(0, y);
          x_device(nx - 1, j) = fun((nx - 1) * hx, y);
        });

    ////////////////////////////////////////////////////////////
    // RHS
    ////////////////////////////////////////////////////////////

    Kokkos::parallel_for(
        "LaplaceOp", DeviceRangeRank2({start_x, start_y}, {end_x, end_y}),
        KOKKOS_LAMBDA(int i, int j) {
          Real x = i * hx;
          Real y = j * hy;
          rhs_device(i, j) = lapl_fun(x, y);
        });

    ////////////////////////////////////////////////////////////
    // Solve
    ////////////////////////////////////////////////////////////

    // Mesh quantities
    Real scale_diag = -(2 / (hx * hx) + 2 / (hy * hy));
    Real hx2 = hx * hx;
    Real hy2 = hy * hy;

    for (int iter = 0; iter < max_iter; ++iter) {

      ////////////////////////////////////////////////////////////
      // Red-black Gauss-Seidel step
      ////////////////////////////////////////////////////////////
      // TODO Implementation of the red-black Gauss-Seidel method. Note that
      // we have two colors (hence two independent sets), consequently we need
      // two parallel_for calls for avoiding read-write conflics. HINT: We read
      // from x_device and write to x_device within the parallel for.

      if (iter % check_residual_each == 0 || (iter == (max_iter - 1))) {
        Real g_norm = 0;

        // Matrix-free residual

        Kokkos::parallel_reduce(
            "SquaredNormOfGradient",
            DeviceRangeRank2({start_x, start_y}, {end_x, end_y}),
            KOKKOS_LAMBDA(int i, int j, Real &acc) {
              Real diag = scale_diag * x_device(i, j);

              Real off_diag = (x_device(i - 1, j) + x_device(i + 1, j)) / hx2 +
                              (x_device(i, j - 1) + x_device(i, j + 1)) / hy2;

              Real res = (rhs_device(i, j) - (diag + off_diag));

              acc += res * res;
            },
            g_norm);

        g_norm = std::sqrt(g_norm);

        printf("Iteration %d, ||g|| = %g\n", iter, g_norm);

        if (g_norm < atol) {
          break;
        }
      }
    }

    ////////////////////////////////////////////////////////////
    // Output
    ////////////////////////////////////////////////////////////

    Kokkos::fence();
    Real end = t.seconds();
    Real user_time = end - start;

    printf("Device: \"%s\"\n", typeid(DeviceExecutionSpace).name());
    printf("Time: %g (seconds)\n", user_time);

    Real weighted_diff = 0;

    // Compute weighted error norm
    Kokkos::parallel_reduce(
        "WightedDiff", DeviceRangeRank2({start_x, start_y}, {end_x, end_y}),
        KOKKOS_LAMBDA(int i, int j, Real &acc) {
          Real x = i * hx;
          Real y = j * hy;
          Real diff = (x_device(i, j) - fun(x, y)) / fun(x, y);
          acc += diff * diff * (hx * hy);
        },
        weighted_diff);

    printf("WightedDiff: %g \n", std::sqrt(weighted_diff));

    std::ofstream os("red_back_gauss_seidel_matrix_free.raw");
    if (os.good()) {
      ViewDevice2D::HostMirror x_host = Kokkos::create_mirror_view(x_device);
      Kokkos::deep_copy(x_host, x_device);
      auto data = x_host.data();
      size_t n = nx * ny;

      os.write((char *)data, n * sizeof(Real));
    }
  }

  Kokkos::finalize();
  return 0;
}
