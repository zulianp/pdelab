nx=30
ny=30

echo './hw_3 '$nx' '$ny
OMP_PROC_BIND=true ./hw_3 $nx $ny

python3 ../../../examples/utils/plot.py --path=red_back_gauss_seidel_matrix_free.raw --nx=$nx --ny=$ny --output=out.png
