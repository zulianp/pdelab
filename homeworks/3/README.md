# Homework 3: The red-black Gauss-Seidel method

## Deadline
Before the next lecture

## Description

In this assignment you will be implementing a matrix-free Poisson solver in the unit square domain. 
By using 2-dimensional arrays/views, you will perform stencil operations instead of using matrices as in the previous assignment.
The solution method will be the **red-black Gauss-Seidel method**.

Perform the following basic tasks (see **TODO** in the code). You have to **deliver** both **code** and the specified **output files**.

1. Implement the following routines as described in the **TODO** comments in the code
    + Implement a red-black Gauss-Seidel step for computing the correction. Note that you need one parallel_for for each color.
2. Run the code from your build folder:
    + Use the `source ../run.sh` command if you are on your Laptop, or on a compute node on the cluster (i.e., you have used `salloc`)
    + Use `sbatch ../run_Cuda.sbatch` for a larger run on the cluster
3. Deliver a **PDF document** where you report the `total number of iterations`, `final residual`, `solution time` (make sure you are running the `Release` executable), the `WeightedDiff` output, and the corresponding `figures`, for the experiments with sizes:
    1. `nx=30;ny=30`
    2. `nx=51;ny=43`
    3. `nx=300;ny=300`
4. Compare the peformance of your Jacobi solver with your red-black Gauss-Seidel solver. 
    + Make sure you compiled your programs with compiler optimizations enabled (i.e. you configured with `cmake .. -DCMAKE_BUILD_TYPE=Release`). 
    + **The runs have to be performed on the cluster**.
    + Add the solution times of the Jacobi to your document (for all 3 experiments) and report speed-ups (Time Jacobi)/(Time Red-black Gauss Seidel).
5. Comment every code block of your program and describe what is happening.


