
#include "base.hpp"

#include "KokkosBlas1_axpby.hpp"

using Real = double;

int main(int argc, char *argv[]) {
  Kokkos::initialize(argc, argv);

  ////////////////////////////////////////////////////////
  using ViewHost1D = Kokkos::View<Real *, HostMemorySpace>;
  using ViewDevice1D = Kokkos::View<Real *, DeviceMemorySpace>;

  static const int n_experiments = 5;
  int sizes[n_experiments] = {1000, 10000, 100000, 1000000, 10000000};

  printf("User,\t\tKokkosBlas,\tSize\n");

  for (int k = 0; k < n_experiments; ++k) {

    int n = sizes[k];

    ViewDevice1D x_device("XOnDevice", n);
    ViewDevice1D y_device("YOnDevice", n);

    ///////////////////////////////////////////////////////
    // Initialize with data useful for testing
    ///////////////////////////////////////////////////////

    // TODO

    ///////////////////////////////////////////////////////

    Kokkos::fence();

    Kokkos::Timer t;
    Real start = t.seconds();

    ///////////////////////////////////////////////////////
    // Implementation of axpy
    ///////////////////////////////////////////////////////

    // TODO

    ///////////////////////////////////////////////////////

    Real end = t.seconds();
    Real user_time = end - start;

    {

      ///////////////////////////////////////////////////////
      // Test the result of custom axpy
      ///////////////////////////////////////////////////////

      // TODO
      // Check correctness also with assert(...)
      // It the result is not what is expected abort program with
      // Kokkos::abort("<Your error message>");

      ///////////////////////////////////////////////////////
    }

    start = t.seconds();
    ///////////////////////////////////////////////////////
    // Implementation axpy using Kokkos blas
    // (search KokkosKernels Wiki for documentation)
    ///////////////////////////////////////////////////////

    // TODO

    ///////////////////////////////////////////////////////

    end = t.seconds();
    Real kokkos_blas_time = end - start;

    {

      ///////////////////////////////////////////////////////
      // Test the result of Kokkos blas axpy
      ///////////////////////////////////////////////////////

      // TODO
      // Check correctness also with assert(...)
      // It the result is not what is expected abort program with
      // Kokkos::abort("<Your error message>");

      ///////////////////////////////////////////////////////
    }

    // Print CSV table
    printf("%g,\t%g,\t%d\n", user_time, kokkos_blas_time, n);
  }

  Kokkos::finalize();
  return 0;
}
