# Homework 1

## Deadline
Before the next lecture

## Description

Perform the following basic tasks (see **TODO** in the code). You have to **deliver** both **code** and the specified **output files**.

1. Follow the steps described in **Lecture 1** and install all the library dependencies.
2. Run the hello world example and report the output in a text-file called `hello.txt`.
3. Implement a program that performs the `axpy` operation. 
    + `axpy` is a BLAS 1 operation and it is defined as `y(i) += a * x(i)`.
    + The operands `x` and `y` are 1D views and `a` is a `double`.
    + The operation is computed in the device.
    + Write a test that that transfers the values from the device to the host and checks its correctness using `assert`. The code needs to be compiled with debug symbols. Use the CMake option `-DCMAKE_BUILD_TYPE=Debug`.
4. Implement a program that performs `axpy` using `KokkosBlas::axpy`. Compare the performance against your implementation using ` Kokkos::Timer` and report timings in a table for number of entries `n = {1e3, 1e4, 1e5, 1e6, 1e7}`. Use the CMake option `-DCMAKE_BUILD_TYPE=Release`.
5. Comment every code block of your program and describe what is happening (e.g., is the data in the host/device, what is the execution space, ...).
