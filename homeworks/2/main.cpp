
#include "base.hpp"

#include "KokkosBlas1_axpby.hpp"
#include "KokkosBlas1_nrm2.hpp"

#include <fstream>

using Real = double;

using ViewDevice2D = Kokkos::View<Real **, DeviceLayout, DeviceMemorySpace>;

// Analytic function
KOKKOS_INLINE_FUNCTION Real fun(const Real x, const Real y) {
  return (x * (1 - x) * y * (1 - y)) * (x + y);
}

// Analytic Laplacian
KOKKOS_INLINE_FUNCTION Real lapl_fun(const Real x, const Real y) {
  return 2 * (y - 1) * y * (3 * x + y - 1) + 2 * (x - 1) * x * (3 * y + x - 1);
}

int main(int argc, char *argv[]) {
  Kokkos::initialize(argc, argv);

  ////////////////////////////////////////////////////////

  {
    Kokkos::Timer t;
    Real start = t.seconds();

    // Grid parameters
    int nx = 10;
    int ny = 10;

    if (argc == 3) {
      nx = atoi(argv[1]);
      ny = atoi(argv[2]);
    }

    int margin = 1;

    int start_x = margin;
    int end_x = nx - margin;

    int start_y = margin;
    int end_y = ny - margin;

    // Geometry (unit square)
    Real hx = 1. / (nx - 1);
    Real hy = 1. / (ny - 1);

    // Solver parameters
    int max_iter = 20 * nx * ny;
    Real atol = 1e-8;
    int check_residual_each = 1;

    ////////////////////////////////////////////////////////////
    // Memory allocation
    ////////////////////////////////////////////////////////////

    ViewDevice2D x_device("XOnDevice", nx, ny);
    ViewDevice2D correction_device("GradientOnDevice", nx, ny);
    ViewDevice2D rhs_device("RHSOnDevice", nx, ny);

    Kokkos::deep_copy(x_device, 0.);
    Kokkos::deep_copy(correction_device, 0.);

    /////////////////////////////////////////////////////////////////////////
    // Implementation of the boundary conditions (Use fun(x,y) to compute the
    // values on the boundary)
    /////////////////////////////////////////////////////////////////////////

    // TODO Iterate over all boundary nodes (there are 4 boundaries), compute
    // the x,y coordinates and store the evaluation of the fun function.

    /////////////////////////////////////////////////////////////////////////
    // Implementation of the RHS (Use lapl_fun(x,y) to compute the values for
    // the right-hand side)
    /////////////////////////////////////////////////////////////////////////

    // TODO Iterate over all interior nodes, compute the x,y coordinates
    // and store the evaluation of the lapl_fun function.

    ///////////////////////////////////////////////////////////////////////////
    // Solve the linear system A * x = rhs, where A is a matrix-free operator
    ///////////////////////////////////////////////////////////////////////////

    // Mesh quantities
    Real scale_diag = -(2 / (hx * hx) + 2 / (hy * hy));
    Real hx2 = hx * hx;
    Real hy2 = hy * hy;

    for (int iter = 0; iter < max_iter; ++iter) {
      /////////////////////////////////////////////////////////////////////////
      // Implementation of the matrix-free Jacobi sweep for the Poisson problem
      /////////////////////////////////////////////////////////////////////////

      // The Jacobi method in matrix form for iteration k is defined as
      // x_{k+1} = D^{-1} * (rhs - (L + U) * x_{k})
      // We solve for the correction instead, which simplifies it slightly to
      // correction = D^{-1} * (rhs - A * x_{k})
      // x_{k+1} = x_{k} + correction

      /////////////////////////////////////////////////////////////////////////
      // Update solution vector using AXPY
      /////////////////////////////////////////////////////////////////////////

      KokkosBlas::axpy(1, correction_device, x_device);

      /////////////////////////////////////////////////////////////////////////

      if (iter % check_residual_each == 0 || (iter == (max_iter - 1))) {
        Real g_norm =
            1; // This value will be overwritten by the parallel_reduce

        /////////////////////////////////////////////////////////////////////////
        // Implementation of the Matrix-free residual norm
        /////////////////////////////////////////////////////////////////////////

        // TODO: use kokkos parallel_reduce for computing the squared norm of
        // the residual and accumulate the result in g_norm. In matrix form the
        // residual is defined as r = (rhs - A * x_{k}). The squared-norm is
        // defined as the sum of the r_i * r_i. Note that here we have matrix
        // free operations, hence we have to perform every part of the algorithm
        // inside the body of the parallel_reduce. Once the squared-norm is
        // computed use std::sqrt(g_norm).
        // Hint: this kernel is similar to the Jacobi sweep, since they both
        // compute a residual.

        /////////////////////////////////////////////////////////////////////////

        printf("Iteration %d, ||g|| = %g\n", iter, g_norm);

        if (g_norm < atol) {
          break;
        }
      }
    }

    ////////////////////////////////////////////////////////////
    // Output
    ////////////////////////////////////////////////////////////

    Real end = t.seconds();
    Real user_time = end - start;

    printf("Device: \"%s\"\n", typeid(DeviceExecutionSpace).name());
    printf("Time: %g (seconds)\n", user_time);

    Real weighted_diff = 0;

    // Compute weighted error norm
    Kokkos::parallel_reduce(
        "WightedDiff", DeviceRangeRank2({start_x, start_y}, {end_x, end_y}),
        KOKKOS_LAMBDA(int i, int j, Real &acc) {
          Real x = i * hx;
          Real y = j * hy;
          Real diff = (x_device(i, j) - fun(x, y)) / fun(x, y);
          acc += diff * diff * (hx * hy);
        },
        weighted_diff);

    Kokkos::fence();

    printf("WightedDiff: %g \n", std::sqrt(weighted_diff));

    std::ofstream os("matrix_free.raw");
    if (os.good()) {
      ViewDevice2D::HostMirror x_host = Kokkos::create_mirror_view(x_device);
      Kokkos::deep_copy(x_host, x_device);
      auto data = x_host.data();
      size_t n = nx * ny;

      os.write((char *)data, n * sizeof(Real));
    }
  }

  Kokkos::finalize();
  return 0;
}
