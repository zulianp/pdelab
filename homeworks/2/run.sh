nx=30
ny=30

echo './hw_2 '$nx' '$ny
OMP_PROC_BIND=true ./hw_2 $nx $ny

python3 ../../../examples/utils/plot.py  --path=matrix_free.raw --nx=$nx --ny=$ny --output=out.png