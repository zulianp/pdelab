# Homework 2: Matrix-free Poisson solver in the unit square

## Deadline
Before the next lecture

## Description

In this assignment you will be implementing a matrix-free Poisson solver in the unit square domain. 
By using 2-dimensional arrays/views, you will perform stencil operations instead of using matrices. 

- For accessing the value stored in a view `x` for a node at coordinate `(i,j)` you can simply write `x(i,j)`. 
- In order to access the neighbors of node `(i,j)` you can offset by `1` or `-1` in any direction. For instance if you want to compute an average of the neighborhood of node `(i,j)` you can write `(x(i-1,j) + x(i+1,j) + x(i, j-1) + x(i, j+1))/4`.

Perform the following basic tasks (see **TODO** in the code). You have to **deliver** both **code** and the specified **output files**.

1. Implement the following routines as described in the **TODO** comments in the code
    + Application of Boundary conditions for all sides of the square
    + Right-hand side computation in the interior of the square
    + Implement a matrix-free Jacobi step for computing the correction
    + Implement a matrix-free computation of the residual norm
2. Run the code from your build folder:
    + Use the `source ../run.sh` command if you are on your Laptop, or on a compute node on the cluster (i.e., you have used `salloc`)
    + Use `sbatch ../run_Cuda.sbatch` for a larger run on the cluster
3. Deliver a **PDF document** where you report the `total number of iterations`, `final residual`, `solution time` (make sure you are running the `Release` executable), the `WeightedDiff` output, and the corresponding `figures`, for the experiments with sizes:
    1. `nx=30;ny=30`
    2. `nx=51;ny=43`
    3. `nx=300;ny=300`
4. Comment every code block of your program and describe what is happening (e.g., which boundary you are setting, why did you choose a specific range to iterate on, ...).


## Hints
- The norm of the residual should be reduced with increasing number of iterations, and it should reach the desired tolerance without needing the maximum number of iterations specified.
- Use `hx` and `hy` to compute the (`x`,`y`) coordinates out of (`i`, `j`) indices.
- The `WeightedDiff` value is a rough measure of the error, it should be much smaller than `1`.
- If we do not want to modify the values of `x` at the boundary in our kernel we restrict our range to internal nodes only. 
