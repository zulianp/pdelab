import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import sys, getopt
import string
import os

def main(argv):
	path = "../assets/drawn_map.png"
	output_path = "../assets/maps/conductivity_map.raw"
	help_message = 'python generate_conductivity.py --path=<path to image> --output=<path to output raw file>'
	is_gpu = False
	kappa=0.0952984

	try:
		opts, args = getopt.getopt(
			argv,"hp:o:",
			["help", "path=", "output=", "gpu", "kappa="])
	except getopt.GetoptError as err:
		print(err)
		print(help_message)
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('-h', '--help'):
			print(help_message)
			sys.exit()
		elif opt in ("-p", "--path"):
			path = arg
		elif opt in ("-o", "--output"):
			output_path = arg
		elif opt in ("--gpu"):
			is_gpu = True
		elif opt in ("--kappa"):
			kappa = float(arg)

	im = plt.imread(path)

	a = im[:,:,0] + im[:,:,1] + im[:,:,2]
	a = a/np.max(a)
	a = 1 - a
	a = kappa * a + 1e-4
	a = a.astype(np.float64)

	if is_gpu:
		print("Using GPU layout")
	else:
		a = np.transpose(a)

	print("Size:")
	print(a.shape)

	binary_file_out = open(output_path, "wb")
	binary_file_out.write(a.tobytes())
	binary_file_out.close()

	print("Generated conductivity file %s, from %s" % (output_path, path))

if __name__ == '__main__':
	main(sys.argv[1:])

