# Project

The project will serve as a final examination for the course together with a presentation in front of the class short interrogation with the examiners.

**Option 1** 

- **Monodomain model** with **IMEX time integration** with **Neumann boundary conditions**
- With **2 or more extensions** from the following list
    * Heterogeneous parameters
    * Non-uniform mesh
    * 3D version
    * Periodic boundary
    
**Option 2** 

- Student personal project (discussed in advance with lecturers)

## Grading

Total 110% 

- **Assignments**                   (35%)
- **Project**                       (75%)
    + **Code**                      (20%)
        * Correctness
        * Performance
        * Clarity and comments
    + **Report and results**        (20%)
        * Correctness
        * Interesting results
        * Performance measurements
        * Form
    + **Presentation**              (15%)
        * Clarity
        * Completeness
    + **Oral exam**                 (20%)
        * Correctness
        

# Resources

For projects with 3D data, you will need to download [Paraview](https://www.paraview.org/download/).

Procedure:

0. Open paraview
1. File > Open
2. Choose Image Reader
3. Fill up the properties panel. Example for a grid with `nx ny nz` = `20 30 40` grid points (i.e., `19 29 39` cells):
    - Data Scalar Type: double
    - Data Byte Order: LittleEndian
    - File Dimensionality: 3
    - Data Extent
        + 0, 19
        + 0, 29
        + 0, 39

See the folder `tutorials/paraview` for figures of the different steps.

### Data ordering

Depending on the memory layout you might need to transpose the data. You can use the Python script `scripts/transpose_3D_data.py`.


