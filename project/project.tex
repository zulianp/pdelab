\documentclass{article}

\usepackage{kpfonts}

\usepackage{amsmath}

\usepackage{color}

\begin{document}

\begin{center}
{ \bf \Large Final project}
\end{center}


Let $\Omega$ be a box domain $(0,L_x)\times(0,L_y)$.
Let $\mathscr I=(0,T_f)$ be a time interval.
We employ the following notation
\begin{itemize}
\item $\mathbf x=(x,y)$
\item $\mathbf n$ the outward normal
\end{itemize}


The objective is to write a finite difference solver for the monodomain problem

\begin{equation}\label{eq:model}
\left\{
\begin{array}{r c l c c l}
  \frac{\partial u}{\partial t} & = &  D u & -& f(u) & \text{in } \Omega \times  \mathscr I\\[2mm]
\mathbf K \nabla u \cdot \mathbf n & = & 0 & & &\text{on } \partial \Omega \times  \mathscr I\\[2mm]
u(\,\cdot\, , 0 ) & = & u_0(\, \cdot \,) & & &\text{in } \Omega
 \end{array}
\right.
\end{equation}
where
\begin{itemize}
\item $u=u(\mathbf x,t)$
\item $f(u) = \frac{a}{\chi} (u - u_r ) (u - u_t) (u- u_d)$ FitzHugh-Nagumo,
\item $Du=\nabla \cdot \mathbf K \nabla u$% = (k^1 u_{x^1})_{x^1}$,
\item $\mathbf K$ is a diagonal matrix with diagonal entries $k_x$ and $ k_y$,
\item $\sigma/(C_m\chi)$, $a/ \chi$, $u_r$, $u_t$, $u_d$ $\in \mathbb R$.
\end{itemize}

In each direction introduce a one-dimensional mesh with $N_x$ and $N_y$ elements.
In each direction, call the nodes $x_i$ and $y_j$,
and use the standard notation of the course, e.g., $E^x_i=(x_i,x_{i+1})$.
Define the tensor product nodes as $\mathbf x_{i,j}=(x_i,y_j)$.


\begin{itemize}
\item Write explicitly the differential operator $D$ assuming heterogeneous conductivities $k_x$ and $k_y$,
i.e., they are not constant in space,
\item For each component of the operator $D$, write the one-dimensional finite difference approximation
with one of the following:
\begin{itemize}
\item[a)] extend the domain to 3D,
\item[b)] assume the mesh is non-uniform in each direction,
\item[c)] assume the improbabilities have a different value on each element.
\end{itemize}
For the two boundary nodes, use the ghost-node technique.
\item Following the hypothesis you chose, write explicitly the operator $D$,
specify the approximation for the interior, the edges, and the corner nodes.
\item with the approximation you derived, write the FD method
 to compute a numerical approximation to \eqref{eq:model}.
 \item Introduce the IMEX scheme with time step $\Delta t$, specifying the matrix and the rhs of the linear system to be solved at each time-step.
\item Implement the method and chosen components starting from the code basis developed from the assignments.
\item Setting $\alpha=0$ the problem degenerates to the heat-equation. Perform the following test and comment on the result:
\begin{itemize}
  \item Implement and use homogeneous Neumann boundary (as specified in~\eqref{eq:model}) conditions on each boundary and corner.
  \item Impose a non-constant initial condition (e.g., $x \cdot y$).
  \item Compute the average of the solution at each time-step.
  \item Verify that the average stays constant.
  \item Document and comment on the results.
\end{itemize}
\item Run simulations with the mondomain model with the following numerical parameters and material properties.
 
 \begin{tabular}{ | c | r  | c |}
$ \sigma_e$ & $0.62$ & mS$\times$mm$^{-1}$ \\
$ \sigma_i$ & $0.17$ & mS$\times$mm$^{-1}$ \\
$ \sigma $ & $\frac{\sigma_i \sigma_e}{\sigma_i+\sigma_e}$ &  mS$\times$mm$^{-1}$ \\
$C_m$ & $0.01$ & $\mu$F$\times$mm$^{-2}$\\
$\chi$ & $140$ & mm$^{-1}$ \\
$a$ & $1.4 \times 10^{-5}$ & \\ 
$u_r$ & -85 & mV\\
$u_t$ & 57.6 & mV\\
$u_d$ & 30 & mV\\
$L_x$ & 20 & mm\\
$L_y$ & 7 & mm\\
\end{tabular}

\begin{itemize}
\item $\Delta t \in \{ 0.05, 0.01, 0.005  \}$
\item $(N_x,N_y) \in \{  (40,14) , (100,35) , (200,70) \}$
\end{itemize}
 
 Depending on the extension you chose
 \begin{itemize}
\item[a)] set $L_z=3$ mm and $N_z$ in such a way that the mesh is uniform
\item[b)] set the nodes as
$$p_i = \left[0.5-0.5\cos \left(\frac{i \pi }{N_p} \right) \right] L_p$$
\item[c)] set a stripe with conductivity $\sigma/4$ in $0.8<x<1.2$ or in $2.5<y<4.5$
 \end{itemize}
 
 For each of the possible choices of meshes and time-step sizes,
 prepare
 a table with the activation time, i.e., the time at which the potential $u$ has a value larger than $u_t$ for all the nodes.
 
 \item Prepare interesting results and respective visualizations (figures, videos) for your report and presentation. Report time to solution (TTS) for different problem sizes (in the order of $10^2, 10^3, 10^4, 10^5$ or more) and present them in properly laid-out tables.

\item \textbf{Upload code, report, and presentation to \emph{icorsi} by the 28th of January, 12:00 AM}.

\end{itemize}


\newpage

\begin{center}
Finite difference approximations for heterogeneous material properties
\end{center}

Let us consider a grid with elements $E_i=(x_i,x_{i+1})$.
We assume all elements have size $h$.
On each element, we assume a constant diffusivity (i.e., conductivity for this assignement) $K_i$.
We would like to approximate the operator

$$\frac{\mathrm{d}}{\mathrm{d}x} \left( K  \frac{\mathrm{d}u}{\mathrm{d}x} \right).$$

First of all, we compute the approximation of the \lq\lq inner\rq\rq~first derivative at the middle node $x_{i+\frac{1}{2}}$ as

$$\frac{\mathrm{d} u}{\mathrm{d} x}(x_{i+\frac{1}{2}}) \approx u^\prime_{i+\frac{1}{2}} = \frac{u_{i+1}-u_i}{h}.$$

Then, we multiply the \lq\lq inner\rq\rq~derivative by $K_i$, i.e.

$$  \left( K \frac{\mathrm{d} u}{\mathrm{d} x}(x_{i+\frac{1}{2}})  \right) \approx K_i u^\prime_{i+\frac{1}{2}} = K_i \frac{u_{i+1}-u_i}{h}.$$

At this point, we can compute an approximation of the second \lq\lq outer\rq\rq~derivative by

\begin{equation}
\begin{split}
\frac{\mathrm{d}}{\mathrm{d}x} \left( K  \frac{\mathrm{d}u}{\mathrm{d}x} \right) (x_i)  \approx & \\
& \frac{ K_i u^\prime_{i+\frac{1}{2}} - K_{i-1} u^\prime_{i-\frac{1}{2}} }{h} = \\
&\frac{
K_i  \frac{u_{i+1}-u_i }{h} -K_{i-1} \frac{u_{i}-u_{i-1} }{h}
}{h} =\\
&\frac{1}{h^2} \left( K_i u_{i+1} - (K_i + K_{i-1}) u_i + K_{i-1} u_{i-1} \right).
\end{split}
\end{equation}

Observe that:
\begin{itemize}
\item if $K_i=1$ for all $i$, we recover the standard approximation for the second derivative
\item if $u_i=1$ for all $i$, the derivative is exact, i.e., it is zero
\item is you want to recover the approximation for non-uniform meshes,
you can proceed just setting a non constant $h_i$.
\end{itemize}


\end{document}