#include <fstream>

void read(const std::string &path, ViewDevice2D &x_device) {
  std::ifstream is(path);
  if (is.good()) {
    ViewDevice2D::HostMirror x_host = Kokkos::create_mirror_view(x_device);

    int nx = x_device.extent(0);
    int ny = x_device.extent(1);

    auto data = x_host.data();
    size_t n = nx * ny;

    is.read((char *)data, n * sizeof(Real));
    is.close();

    Kokkos::deep_copy(x_device, x_host);
  } else {
    std::cerr << "[Error] failed to read file at: " << path << std::endl;
    Kokkos::abort("Aborting!");
  }
}