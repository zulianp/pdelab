#ifndef READ_FILE_HPP
#define READ_FILE_HPP

// Our standard base file
#include "base.hpp"

// Required STL headers
#include <fstream>
#include <string>

// Type definitions (you might have them already elsewhere)
using Real = double;
using ViewDevice1D = Kokkos::View<Real *, DeviceMemorySpace>;
using ViewDevice2D = Kokkos::View<Real **, DeviceMemorySpace>;

/// Example function for reading binary data from file. Assumes x_device has the
/// correct sizes.
void read(const std::string &path, ViewDevice2D &x_device);

#endif // READ_FILE_HPP