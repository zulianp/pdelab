nx=30
ny=30

echo './poisson '$nx' '$ny
OMP_PROC_BIND=true ./poisson $nx $ny

python3 ../../utils/plot.py --path=matrix_based.raw --nx=$nx --ny=$ny --output=out.png
