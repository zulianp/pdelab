
#include "base.hpp"

#include "KokkosBlas1_axpby.hpp"
#include "KokkosBlas1_mult.hpp"
#include "KokkosBlas1_nrm2.hpp"
#include "KokkosKernels_SparseUtils.hpp"
#include "KokkosSparse_CrsMatrix.hpp"
#include "KokkosSparse_spmv.hpp"

#include <fstream>

using Real = double;

// 1D arrays
using ViewDevice1D = Kokkos::View<Real *, DeviceMemorySpace>;

// CrsMatrix
using CrsMatrixDevice = KokkosSparse::CrsMatrix<Real, int, DeviceMemorySpace>;

// CrsMatrix inner types
using CrsGraphDevice = CrsMatrixDevice::staticcrsgraph_type;
using CrsRowPtrDevice = CrsGraphDevice::row_map_type::non_const_type;
using CrsColIdxDevice = CrsGraphDevice::entries_type::non_const_type;
using CrsValuesDevice = CrsMatrixDevice::values_type::non_const_type;

// Analytic function
KOKKOS_INLINE_FUNCTION Real fun(const Real x, const Real y) {
  return (x * (1 - x) * y * (1 - y)) * (x + y);
}

// Analytic Laplacian
KOKKOS_INLINE_FUNCTION Real lapl_fun(const Real x, const Real y) {
  return 2 * (y - 1) * y * (3 * x + y - 1) + 2 * (x - 1) * x * (3 * y + x - 1);
}

/**
 * @brief Basic hardware-portable implementation of a 2D Poisson problem with
 * zero-boundary conditions.
 **/
int main(int argc, char *argv[]) {
  Kokkos::initialize(argc, argv);

  {
    Kokkos::Timer t;
    Real start = t.seconds();

    // Grid parameters
    int nx = 100;
    int ny = 100;

    if (argc == 3) {
      nx = atoi(argv[1]);
      ny = atoi(argv[2]);
    }

    int n = nx * ny;
    int margin = 1;

    int start_x = margin;
    int end_x = nx - margin;

    int start_y = margin;
    int end_y = ny - margin;

    // Geometry (unit square)
    Real hx = 1. / (nx - 1);
    Real hy = 1. / (ny - 1);

    // Matrix pattern
    int nnz_x_row = 5;

    // Solver parameters
    int max_iter = n * 20;
    Real atol = 1e-8;
    int check_residual_each = 500;

    // extras
    bool print_matrix = false;

    ////////////////////////////////////////////////////////////
    // Memory allocation
    ////////////////////////////////////////////////////////////

    ViewDevice1D x_device("XOnDevice", n);
    ViewDevice1D g_device("GradientOnDevice", n);
    ViewDevice1D rhs_device("RHSOnDevice", n);
    ViewDevice1D inv_diag("InverseDiagonal", n);

    Kokkos::deep_copy(x_device, 0.);
    Kokkos::deep_copy(g_device, 0.);

    CrsRowPtrDevice row_ptr("RowPtr", n + 1);
    CrsColIdxDevice col_idx("ColIdx", n * nnz_x_row);
    CrsValuesDevice values("CrsValues", n * nnz_x_row);

    ////////////////////////////////////////////////////////////
    // Populating the CRS matrix (inner part)
    ////////////////////////////////////////////////////////////

    // From 2D to 1D
    // C(i, j) = i * ny + j

    Kokkos::parallel_for(
        "LaplaceMatrix_Inner",
        DeviceRangeRank2({start_x, start_y}, {end_x, end_y}),
        KOKKOS_LAMBDA(int i, int j) {
          int row = i * ny + j;

          row_ptr(row + 1) = (row + 1) * nnz_x_row;

          int offset = row * nnz_x_row;

          // Indices
          col_idx(offset) = (i - 1) * ny + j;     // C(i-1, j)
          col_idx(offset + 1) = i * ny + (j - 1); // C(i, j-1)
          col_idx(offset + 2) = row;              // C(i, j)
          col_idx(offset + 3) = i * ny + (j + 1); // C(i, j+1)
          col_idx(offset + 4) = (i + 1) * ny + j; // C(i+1, j)

          // Values
          values(offset) = 1 / (hx * hx);
          values(offset + 1) = 1 / (hy * hy);
          values(offset + 2) = -(2 / (hx * hx) + 2 / (hy * hy));
          values(offset + 3) = 1 / (hy * hy);
          values(offset + 4) = 1 / (hx * hx);
        });

    Kokkos::fence();

    ////////////////////////////////////////////////////////////
    // Populating the CRS matrix (boundary part)
    ////////////////////////////////////////////////////////////

    Kokkos::parallel_for(
        "LaplaceMatrix_Boundary_X", DeviceRangeRank1(0, nx),
        KOKKOS_LAMBDA(int i) {
          int row_bottom = i * ny;
          int row_top = i * ny + (ny - 1);

          row_ptr(row_bottom + 1) = (row_bottom + 1) * nnz_x_row;
          row_ptr(row_top + 1) = (row_top + 1) * nnz_x_row;

          int offset_bottom = row_bottom * nnz_x_row;
          int offset_top = row_top * nnz_x_row;

          for (int i = 0; i < nnz_x_row; ++i) {
            // Indices
            // point to the same index for reducing memory access
            col_idx(offset_bottom + i) = row_bottom;
            col_idx(offset_top + i) = row_top;

            // Values
            // set zeros
            values(offset_bottom + i) = 0;
            values(offset_top + i) = 0;
          }

          // Values
          values(offset_bottom + 2) = 1;
          values(offset_top + 2) = 1;
        });

    Kokkos::fence();

    Kokkos::parallel_for(
        "LaplaceMatrix_Boundary_Y", DeviceRangeRank1(start_y, end_y),
        KOKKOS_LAMBDA(int j) {
          int row_left = j;
          int row_right = (nx - 1) * ny + j;

          assert(row_left > 0);

          row_ptr(row_left + 1) = (row_left + 1) * nnz_x_row;
          row_ptr(row_right + 1) = (row_right + 1) * nnz_x_row;

          int offset_left = row_left * nnz_x_row;
          int offset_right = row_right * nnz_x_row;

          for (int i = 0; i < nnz_x_row; ++i) {
            // Indices
            // point to the same index for reducing memory access
            col_idx(offset_left + i) = row_left;
            col_idx(offset_right + i) = row_right;

            // Values
            // set zeros
            values(offset_left + i) = 0;
            values(offset_right + i) = 0;
          }

          // Values
          values(offset_left + 2) = 1;
          values(offset_right + 2) = 1;
        });

    Kokkos::fence();

    ////////////////////////////////////////////////////////////
    // Compute Diagonal inverse
    ////////////////////////////////////////////////////////////

    Kokkos::parallel_for(
        "ComputeInverseDiagonal", DeviceRangeRank1(0, n),
        KOKKOS_LAMBDA(int i) { inv_diag(i) = 1. / values(i * nnz_x_row + 2); });

    Kokkos::fence();

    CrsGraphDevice graph(col_idx, row_ptr);
    CrsMatrixDevice matrix("CrsMatrix", n, values, graph);

    ////////////////////////////////////////////////////////////
    // Print CRS Matrix
    ////////////////////////////////////////////////////////////

    if (print_matrix) {
      Kokkos::parallel_for(
          "PrintMatrix", DeviceRangeRank1(0, n), KOKKOS_LAMBDA(int i) {
            int row_begin = row_ptr(i);
            int row_end = row_ptr(i + 1);

            printf("row(%d)[%d, %d]\n", i, row_begin, row_end);
            printf("(%d, %d, %d, %d, %d)\n", col_idx[row_begin],
                   col_idx[row_begin + 1], col_idx[row_begin + 2],
                   col_idx[row_begin + 3], col_idx[row_begin + 4]);

            printf("(%g, %g, %g, %g, %g)\n", values[row_begin],
                   values[row_begin + 1], values[row_begin + 2],
                   values[row_begin + 3], values[row_begin + 4]);
          });

      Kokkos::fence();
    }

    ////////////////////////////////////////////////////////////
    // Boundary conditions
    ////////////////////////////////////////////////////////////

    Kokkos::parallel_for(
        "BoundaryConditions_X", DeviceRangeRank1(0, nx), KOKKOS_LAMBDA(int i) {
          Real x = i * hx;
          rhs_device(i * ny) = fun(x, 0);
          rhs_device(i * ny + ny - 1) = fun(x, (ny - 1) * hy);
        });

    Kokkos::parallel_for(
        "BoundaryConditions_Y", DeviceRangeRank1(0, ny), KOKKOS_LAMBDA(int j) {
          Real y = j * hy;
          rhs_device(j) = fun(0, y);
          rhs_device((nx - 1) * ny + j) = fun((nx - 1) * hx, y);
        });

    ////////////////////////////////////////////////////////////
    // RHS
    ////////////////////////////////////////////////////////////

    Kokkos::parallel_for(
        "LaplaceOp", DeviceRangeRank2({start_x, start_y}, {end_x, end_y}),
        KOKKOS_LAMBDA(int i, int j) {
          Real x = i * hx;
          Real y = j * hy;
          rhs_device(i * ny + j) = lapl_fun(x, y);
        });

    Kokkos::fence();

    ////////////////////////////////////////////////////////////
    // Solve
    ////////////////////////////////////////////////////////////

    for (int iter = 0; iter < max_iter; ++iter) {
      // Compute residual
      Kokkos::deep_copy(g_device, rhs_device);
      KokkosSparse::spmv("N", -1, matrix, x_device, 1,
                         g_device); // y = beta*y + alpha*Op(A)*x

      if (iter % check_residual_each == 0 || (iter == (max_iter - 1))) {
        Real g_norm = 0;

        Kokkos::parallel_reduce(
            "NormOfGradient", DeviceRangeRank1(0, n),
            KOKKOS_LAMBDA(int i, Real &acc) {
              acc += g_device(i) * g_device(i);
            },
            g_norm);

        g_norm = std::sqrt(g_norm);

        printf("Iteration %d, ||g|| = %g\n", iter, g_norm);

        // Check if we reached desired absolute tolerance
        if (g_norm < atol) {
          // Exit solver loop
          break;
        }
      }

      // Jacobi step
      Kokkos::parallel_for(
          "JacobiStep", DeviceRangeRank1(0, n),
          KOKKOS_LAMBDA(int i) { g_device(i) *= inv_diag(i); });

      // Gradient descent.
      // Consider quadratic function x^T * A * x - x^T * b,
      // its gradient is A * x - b. Hence a gradient descent step implementation
      // would be: x -= learning_rate (A * x - b)
      // Kokkos::parallel_for(
      //     "GradientDescentStep", DeviceRangeRank1(0, n), KOKKOS_LAMBDA(int i)
      //     {
      //       Real learning_rate = 0.00001;
      //       g_device(i) *= -learning_rate;
      //     });

      // Correction step
      KokkosBlas::axpy(1, g_device, x_device);
    }

    ////////////////////////////////////////////////////////////
    // Output
    ////////////////////////////////////////////////////////////

    Real end = t.seconds();
    Real user_time = end - start;

    printf("Device: \"%s\"\n", typeid(DeviceExecutionSpace).name());
    printf("Time: %g (seconds)\n", user_time);

    Real weighted_diff = 0;

    // Compute weighted error norm
    Kokkos::parallel_reduce(
        "WightedDiff", DeviceRangeRank2({start_x, start_y}, {end_x, end_y}),
        KOKKOS_LAMBDA(int i, int j, Real &acc) {
          Real x = i * hx;
          Real y = j * hy;
          Real diff = (x_device(i * ny + j) - fun(x, y)) / fun(x, y);
          acc += diff * diff * (hx * hy);
        },
        weighted_diff);

    Kokkos::fence();

    printf("WightedDiff: %g \n", std::sqrt(weighted_diff));

    std::ofstream os("matrix_based.raw");
    if (os.good()) {
      ViewDevice1D::HostMirror x_host = Kokkos::create_mirror_view(x_device);
      Kokkos::deep_copy(x_host, x_device);

      auto data = x_host.data();

      os.write((char *)data, n * sizeof(Real));
    }
  }

  Kokkos::finalize();
  return 0;
}
