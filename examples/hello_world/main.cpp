#include "base.hpp"

int main(int argc, char *argv[]) {
  Kokkos::initialize(argc, argv);

  ////////////////////////////////////////////////////////

  int n = 10;

  printf("Serial: \"%s\"\n", typeid(Kokkos::Serial).name());
  printf("---------\n");

  Kokkos::parallel_for(
      "ParallelForOnSerial", SerialRangeRank1(0, n),
      KOKKOS_LAMBDA(int i) { printf("Item %d\n", i); });

  printf("----------------------------------------\n");

  ////////////////////////////////////////////////////////

  printf("Device: \"%s\"\n", typeid(DeviceExecutionSpace).name());
  printf("---------\n");

  Kokkos::parallel_for(
      "ParallelForOnDevice", DeviceRangeRank1(0, n),
      KOKKOS_LAMBDA(int i) { printf("Item %d\n", i); });

  // Wait for cuda kernel to finish
  Kokkos::fence();
  printf("----------------------------------------\n");

  ////////////////////////////////////////////////////////

  printf("Host: \"%s\"\n", typeid(HostExecutionSpace).name());
  printf("---------\n");

  Kokkos::parallel_for(
      "ParallelForOnHost", HostRangeRank1(0, n),
      KOKKOS_LAMBDA(int i) { printf("Item %d\n", i); });

  printf("----------------------------------------\n");

  Kokkos::finalize();
  return 0;
}
