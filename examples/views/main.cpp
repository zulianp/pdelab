#include "base.hpp"

int main(int argc, char *argv[]) {

  Kokkos::initialize(argc, argv);

  ////////////////////////////////////////////////////////
  // 1D
  { // Scope for limiting views lifetime
    using ViewHost1D = Kokkos::View<double *, HostMemorySpace>;
    using ViewDevice1D = Kokkos::View<double *, DeviceMemorySpace>;

    printf("---------------------\n");
    printf("1D\n");

    int n = 10;

    ViewDevice1D data_device("DataOnDevice", n);
    ViewHost1D data_host = Kokkos::create_mirror_view(data_device);

    Kokkos::parallel_for(
        "ParallelForOnDevice", DeviceRangeRank1(0, n),
        KOKKOS_LAMBDA(int i) { data_device(i) = i * i; });

    Kokkos::deep_copy(data_host, data_device);

    // Host-data can be traversed in without any parallel_for
    for (int i = 0; i < n; ++i) {
      printf("%g\n", data_host(i));
    }

    printf("---------------------\n");
  }
  ////////////////////////////////////////////////////////

  // 2D
  { // Scope for limiting views lifetime
    using ViewHost2D = Kokkos::View<double **, HostLayout, HostMemorySpace>;
    using ViewDevice2D =
        Kokkos::View<double **, DeviceLayout, DeviceMemorySpace>;

    printf("---------------------\n");
    printf("2D\n");

    int nx = 2;
    int ny = 3;

    ViewDevice2D data_device("DataOnDevice", nx, ny);
    ViewDevice2D::HostMirror data_host =
        Kokkos::create_mirror_view(data_device);

    printf("Matrix access (device)\n");
    Kokkos::parallel_for(
        "ParallelForOnDevice", DeviceRangeRank2({0, 0}, {nx, ny}),
        KOKKOS_LAMBDA(int i, int j) {
          data_device(i, j) = i * ny + j;
          printf("%g\n", data_device(i, j));
        });

    Kokkos::fence();

    Kokkos::deep_copy(data_host, data_device);

    ////////////////////////////////////////////////////////////////
    // Host-data can be traversed without any parallel_for.
    // If you want to be serial.
    ////////////////////////////////////////////////////////////////

    printf("Matrix access (host mirror)\n");
    for (int i = 0; i < nx; ++i) {
      for (int j = 0; j < ny; ++j) {
        printf("%g\n", data_host(i, j));
      }
    }

    printf("Raw access\n");
    int n = nx * ny;
    auto ptr = data_host.data();
    for (int i = 0; i < n; ++i) {
      printf("%g\n", ptr[i]);
    }
  }

  Kokkos::finalize();
  return 0;
}
