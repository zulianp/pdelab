nx=30
ny=30

nx=51
ny=43

nx=300
ny=300

echo './redblackgs '$nx' '$ny
OMP_PROC_BIND=true ./redblackgs $nx $ny

python3 ../../utils/plot.py --path=red_back_gauss_seidel.raw --nx=$nx --ny=$ny --output=out.png